package com.tp.adel.android2;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.NotificationCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by adel on 24.01.16.
 */
public class LoginFragment extends Fragment implements View.OnClickListener {
    private static int counter = 0;
    private static int numMessages = 0;
    private EditText emailEditText;
    private EditText loginEditText;
    private EditText passEditText;
    private SharedPreferences mSharedPreferences;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ((MainActivity) getActivity() ).unsetFullScreenFlag();
        View rootView = inflater.inflate(R.layout.login, container, false);
        emailEditText = (EditText) rootView.findViewById(R.id.email);
        loginEditText = (EditText) rootView.findViewById(R.id.login);
        passEditText = (EditText) rootView.findViewById(R.id.pass);
        mSharedPreferences = getActivity().getSharedPreferences("auth_settings",
                Context.MODE_PRIVATE);
        emailEditText.setText(mSharedPreferences.getString("email", ""));
        loginEditText.setText(mSharedPreferences.getString("login", ""));
        passEditText.setText(mSharedPreferences.getString("pass", ""));
        rootView.findViewById(R.id.login_button).setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_button:
                SharedPreferences.Editor sharedPreferencesEditor = mSharedPreferences.edit();
                String email = emailEditText.getText().toString();
                String login = loginEditText.getText().toString();
                String pass = passEditText.getText().toString();
                sharedPreferencesEditor.putString("email", email);
                sharedPreferencesEditor.putString("login", login);
                sharedPreferencesEditor.putString("pass", pass);
                sharedPreferencesEditor.apply();
                Toast.makeText(getActivity(),
                        emailEditText.getText() + " : " + loginEditText.getText() + " : " + passEditText.getText(),
                        Toast.LENGTH_SHORT).show();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragments_container,
                        new ChannelFragment()).addToBackStack(null).commit();


                NotificationManagerCompat mNotificationManager = NotificationManagerCompat.from(getActivity());

                int notifyID = counter++;

                NotificationCompat.Builder mNotifyBuilder = new NotificationCompat.Builder(getActivity());

                      mNotifyBuilder.setContentTitle("New Message")
                        .setContentText("You've received new messages.")
                        .setSmallIcon(R.drawable.ic_announcement_white_24dp)
                        .setGroup("Android2");

                mNotifyBuilder.setContentText(emailEditText.getText() + " : " + loginEditText.getText() + " : " + passEditText.getText())
                        .setNumber(++numMessages);

                mNotificationManager.notify(
                        notifyID,
                        mNotifyBuilder.build());
        }
    }
}

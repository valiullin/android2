package com.tp.adel.android2;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class ChannelFragment extends Fragment {

    public ChannelFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        MainActivity mainActivity = (MainActivity) getActivity();
        ActionBar actionBar = mainActivity.getSupportActionBar();
        assert actionBar != null;
        actionBar.setTitle(R.string.action_channel_list);
        actionBar.setIcon(R.drawable.ic_format_list_numbered_white_24dp);

        return inflater.inflate(R.layout.fragment_channel, container, false);
    }
}

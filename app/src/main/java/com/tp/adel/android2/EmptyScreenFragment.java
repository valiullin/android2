package com.tp.adel.android2;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

public class EmptyScreenFragment extends Fragment {
    private SharedPreferences mSharedPreferences;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mSharedPreferences = getActivity().getSharedPreferences("auth_settings", Context.MODE_PRIVATE);
        String email = mSharedPreferences.getString("email", "");
        String login = mSharedPreferences.getString("login", "");
        String pass = mSharedPreferences.getString("pass", "");
        if (!email.equals("") && !login.equals("") && !pass.equals("")){
            Toast.makeText(getActivity(), email + " : " + login + " : " + pass, Toast.LENGTH_SHORT).show();

        } else {
            Toast.makeText(getActivity(), R.string.noshared, Toast.LENGTH_SHORT).show();
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragments_container, new LoginFragment()).commit();

        }
        return inflater.inflate(R.layout.fragment_splash, container, false);

    }
}

